package com.java.demo;

import com.java.demo.commons.ApiResponse;
import com.java.demo.dtos.UserDto;
import com.java.demo.models.User;
import com.java.demo.repositories.UserRepository;
import com.java.demo.services.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;


    @Test
    public void findAllUsersTest() {
        when(userRepository.findAll()).thenReturn(Stream.of(
                new User(1l,"Shahzad","shahzad.iqbal.developer@gmail.com","[\"Service A\",\"Service B\",\"Service C\"]","Monday 9:00 AM", LocalDate.now()),
                new User(1l,"John","john@gmail.com","[\"Service B\"]","Tuesday 10:00 AM", LocalDate.now()),
                new User(1l,"Doe","doe@gmail.com","[\"Service A\"]","Wednesday 11:00 AM", LocalDate.now())
        ).collect(Collectors.toList()));
        assertEquals(3, userService.getAll().size());
    }

    @Test
    public void saveUserTest() {
        UserDto userDto = new UserDto(1l,"Shahzad","shahzad.iqbal.developer@gmail.com","[\"Service A\",\"Service B\",\"Service C\"]","Monday 9:00 AM", LocalDate.now());
        User user = new User(1l,"Shahzad","shahzad.iqbal.developer@gmail.com","[\"Service A\",\"Service B\",\"Service C\"]","Monday 9:00 AM", LocalDate.now());
        when(userRepository.save(any())).thenReturn(user);
        ApiResponse<User> apiResponse = userService.save(userDto);
        assertEquals(200,apiResponse.getStatus());
    }
}
