package com.java.demo.controllers;

import com.java.demo.dtos.UserDto;
import com.java.demo.commons.ApiResponse;
import com.java.demo.models.User;
import com.java.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "*", maxAge = 3600)
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    public ApiResponse<User> save(@RequestBody UserDto userDto){
        return userService.save(userDto);
    }

    @GetMapping
    public List<User> getAll(){
        return userService.getAll();
    }

}
