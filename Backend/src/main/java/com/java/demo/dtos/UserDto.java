package com.java.demo.dtos;

import java.time.LocalDate;

public class UserDto {

    private Long id;
    private String name;
    private String email;
    private String requestedService;
    private String timeOfEntry;
    private LocalDate createdDate;

    public UserDto(Long id, String name, String email, String requestedService, String timeOfEntry, LocalDate createdDate) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.requestedService = requestedService;
        this.timeOfEntry = timeOfEntry;
        this.createdDate = createdDate;
    }

    public UserDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRequestedService() {
        return requestedService;
    }

    public void setRequestedService(String requestedService) {
        this.requestedService = requestedService;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public String getTimeOfEntry() {
        return timeOfEntry;
    }

    public void setTimeOfEntry(String timeOfEntry) {
        this.timeOfEntry = timeOfEntry;
    }
}
