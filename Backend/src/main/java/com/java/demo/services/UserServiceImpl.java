package com.java.demo.services;

import com.java.demo.commons.ApiResponse;
import com.java.demo.dtos.UserDto;
import com.java.demo.repositories.UserRepository;
import com.java.demo.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JavaMailSender sender;

    @Value("${spring.mail.username}")
    private String username;

    @Override
    public ApiResponse<User> save(UserDto userDto) {
        User user = new User();
        user.setName(userDto.getName());
        user.setEmail(userDto.getEmail());
        user.setRequestedService(userDto.getRequestedService());
        user.setTimeOfEntry(userDto.getTimeOfEntry());
        try {
            User savedUser = userRepository.save(user);
            //sendEmail(user);
            return new ApiResponse(200, "User saved successfully!", savedUser);
        }catch (Exception ex){
            return new ApiResponse(500, "Something went wrong");
        }
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    public void sendEmail(User user) {
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        try {
            helper.setFrom(username);
            helper.setTo(user.getEmail());
            helper.setSubject("Nicholas Testing Mail");
            String messageBody = "Name: "+user.getName()+"\n"+
                    "Entry Time: "+user.getTimeOfEntry()+"\n"+
                    "Services: "+user.getRequestedService();
            helper.setText(messageBody, true);
            sender.send(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
