package com.java.demo.services;

import com.java.demo.commons.ApiResponse;
import com.java.demo.dtos.UserDto;
import com.java.demo.models.User;

import java.util.List;

public interface UserService {

    ApiResponse<User> save(UserDto userDto);

    List<User> getAll();

}
