import './App.css';
import User from '../src/components/User';
import 'react-toastify/dist/ReactToastify.css';
import Admin from './components/Admin';
import { BrowserRouter, Routes, Route } from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<User />} />
        <Route path="admin" element={<Admin />} />
      </Routes>
    </BrowserRouter>
    
  );
}

export default App;
