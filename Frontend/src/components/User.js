import React, { useState } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import { Link } from "react-router-dom";
import api from '../apis/User';

function User() {

    const [user, setUser] = useState({
        name:"",
        email:"",
        requestedService:[],
        day:"",
        time:""
    });

    const handleInput = e => {
        const targetName = e.target.name;
        const value = e.target.value;
        if(targetName === 'requestedService'){
            if(e.target.checked){
                let requestedServiceArray = [...user.requestedService];
                requestedServiceArray.push(value);
                setUser({ ...user, [targetName] : requestedServiceArray });
            }else{
                let requestedServiceArray = [...user.requestedService];
                let ind = requestedServiceArray.findIndex(el=> el === value);
                requestedServiceArray.splice(ind,1);
                setUser({ ...user, [targetName] : requestedServiceArray });
            }
        }else{
            setUser({ ...user, [targetName] : value });
        }
    }

    const handleSubmit = async e => {
        e.preventDefault();
        if(user.day === "" || user.time === "" || user.name === "" || user.email === "" || user.requestedService === ""){
            toast("Please fill all the fields!")
        }else{
            let userDto = {
                name : user.name,
                email : user.email,
                requestedService : JSON.stringify(user.requestedService),
                timeOfEntry : `${user.day} ${user.time}`,
            }
            const response = await api.post(`/user`, userDto);
            if(response.data.status === 200){
                toast(response.data.message);
                let checkBoxes = document.getElementsByName('requestedService');
                for(let checkbox of checkBoxes){
                    checkbox.checked = false;
                }
                setUser({
                    name:"",
                    email:"",
                    requestedService:[],
                    day:"",
                    time:""
                }); 
            }
            
        }
        
    }

  return (
    <>
    <ToastContainer/> 
    <div className="container">
      <div className="row">
        <div className="col-lg-3"></div>
        <div className="col-lg-6">
          <div className="card my-3 p-4">
            <div className='card-title text-center'>
                <h4> User Form (<Link to="admin">Admin</Link>) </h4>
            </div>
            <form onSubmit={handleSubmit}>
                <div className="mb-3">
                    <select className="form-select" value={user.day} name='day' placeholder='Select Day' onChange={handleInput}>
                        <option value="" disabled>Select Day</option>
                        <option value="Monday">Monday</option>
                        <option value="Tuesday">Tuesday</option>
                        <option value="Wednesday">Wednesday</option>
                        <option value="Thursday">Thursday</option>
                        <option value="Friday">Friday</option>
                        <option value="Saturday">Saturday</option>
                        <option value="Sunday">Sunday</option>
                    </select>
                </div>
                <div className="mb-3">
                    <select className="form-select" value={user.time} name='time' placeholder='Select Time' onChange={handleInput}>
                        <option value="" disabled>Select Time</option>
                        <option value="9:00 AM">9:00 AM</option>
                        <option value="10:00 AM">10:00 AM</option>
                        <option value="11:00 AM">11:00 AM</option>
                        <option value="12:00 PM">12:00 PM</option>
                        <option value="01:00 PM">01:00 PM</option>
                        <option value="02:00 PM">02:00 PM</option>
                        <option value="03:00 PM">03:00 PM</option>
                        <option value="04:00 PM">04:00 PM</option>
                        <option value="05:00 PM">05:00 PM</option>
                    </select>
                </div>
                <div className="mb-3">
                    <label htmlFor='name' className="form-label">Name</label>
                    <input type="text" className="form-control" name='name' onChange={handleInput} value={user.name} />
                </div>
                <div className="mb-3">
                    <label htmlFor='email' className="form-label">Email address</label>
                    <input type="email" className="form-control" name='email' onChange={handleInput} value={user.email} />
                </div>
                <div className="mb-3">
                    <div className="form-check">
                        <input className="form-check-input" type="checkbox" name='requestedService' value="Service A" onChange={handleInput}/>
                        <label className="form-check-label" htmlFor="requestedService">
                        Service A
                        </label>
                    </div>
                    <div className="form-check">
                        <input className="form-check-input" type="checkbox" name='requestedService' value="Service B" onChange={handleInput}/>
                        <label className="form-check-label" htmlFor="requestedService">
                        Service B
                        </label>
                    </div>
                    <div className="form-check">
                        <input className="form-check-input" type="checkbox" name='requestedService' value="Service C" onChange={handleInput}/>
                        <label className="form-check-label" htmlFor="requestedService">
                        Service C
                        </label>
                    </div>
                    <div className="form-check">
                        <input className="form-check-input" type="checkbox" name='requestedService' value="Service D" onChange={handleInput}/>
                        <label className="form-check-label" htmlFor="requestedService">
                        Service D
                        </label>
                    </div>
                    <div className="form-check">
                        <input className="form-check-input" type="checkbox" name='requestedService' value="Service E" onChange={handleInput}/>
                        <label className="form-check-label" htmlFor="requestedService">
                        Service E
                        </label>
                    </div>
                </div>
                <div className="mb-3">
                    <button className="btn btn-primary">Save</button>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    </>
  )
}

export default User;