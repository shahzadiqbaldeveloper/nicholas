import React, { useState, useEffect } from 'react';
import { Link } from "react-router-dom";
import api from '../apis/User';

function Admin() {

    const [users, setUsers] = useState([]);

    const getUsers = async () => {
        const response = await api.get("/user");
        return response.data;
    };

    useEffect(() => {
        const getAllUsers = async () => {
            const usersList = await getUsers();
            if (usersList) {
                setUsers(usersList);
            }
        }
    
        getAllUsers();
        
    }, []);

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-lg-12">
            <h4 className='text-center my-4'> Users List (<Link to="/">User</Link>)</h4>
              <table className='table table-bordered table-hover'>
                    <thead>
                        <tr>
                            <th>Time of Entry</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Services</th>
                        </tr>
                    </thead>
                    <tbody>
                        {users.map(user=>(
                            <tr key={user.id}>
                                <td>{user.timeOfEntry}</td>
                                <td>{user.name}</td>
                                <td>{user.email}</td>
                                <td>{user.requestedService}</td>
                            </tr>
                        )
                        )}
                    </tbody>
              </table>
        </div>
      </div>
    </div>
  )
}

export default Admin;